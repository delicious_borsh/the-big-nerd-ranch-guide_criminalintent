package com.ponykamni.criminalintent

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.Room
import com.ponykamni.criminalintent.database.CrimeDatabase
import com.ponykamni.criminalintent.database.migration_1_2
import java.io.File
import java.util.*
import java.util.concurrent.Executors

class CrimeRepository private constructor(context: Context) {

    private val database: CrimeDatabase = Room.databaseBuilder(
        context.applicationContext,
        CrimeDatabase::class.java,
        DATABASE_NAME
    )
        .addMigrations(migration_1_2)
        .build()

    private val crimeDao = database.getDao()

    private val executor = Executors.newSingleThreadExecutor()
    private val filesDir = context.applicationContext.filesDir

    fun getCrimes(): LiveData<List<Crime>> = crimeDao.getCrimes()
    fun getCrime(id: UUID): LiveData<Crime?> = crimeDao.getCrime(id)

    fun deleteCrime(crime: Crime) {
        executor.execute {
            crimeDao.deleteCrime(crime)
        }
    }

    fun createOrUpdateCrime(crime: Crime) {
        executor.execute {
            crimeDao.insertOrUpdate(crime)
        }
    }

    fun getPhotoFile(photoFileName: String): File = File(filesDir,photoFileName)

    companion object {

        private const val DATABASE_NAME = "crime-database"

        private var INSTANCE: CrimeRepository? = null
        fun initialize(context: Context) {
            if (INSTANCE == null) {
                INSTANCE = CrimeRepository(context)
            }
        }

        fun get(): CrimeRepository {
            return INSTANCE ?: throw IllegalStateException("CrimeRepository must be initialized")
        }
    }
}