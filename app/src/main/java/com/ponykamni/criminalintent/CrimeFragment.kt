package com.ponykamni.criminalintent

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import java.io.File
import java.text.DateFormat
import java.util.*

class CrimeFragment : Fragment(), DatePickerFragment.Callback, TimePickerFragment.Callback {

    private lateinit var crime: Crime
    private lateinit var photoFile: File
    private lateinit var photoUri: Uri

    private lateinit var titleField: EditText
    private lateinit var dateField: TextView

    private lateinit var changeDate: Button
    private lateinit var changeTime: Button

    private lateinit var solvedCheckBox: CheckBox
    private lateinit var reportButton: Button

    private lateinit var suspectButton: Button

    private lateinit var photoButton: ImageButton
    private lateinit var photoView: ImageView

    private lateinit var saveButton: Button

    private val crimeDetailViewModel by lazy {
        ViewModelProvider(this).get(CrimeDetailViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        crime = Crime()
        val crimeId: UUID? = arguments?.getSerializable(ARG_CRIME_ID) as? UUID

        crimeId?.let {
            crimeDetailViewModel.loadCrime(crimeId)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_crime, container, false)

        titleField = view.findViewById(R.id.crime_title) as EditText
        dateField = view.findViewById(R.id.crime_date) as TextView

        changeDate = view.findViewById(R.id.change_date) as Button
        changeTime = view.findViewById(R.id.change_time) as Button

        solvedCheckBox = view.findViewById(R.id.crime_solved) as CheckBox
        reportButton = view.findViewById(R.id.crime_report) as Button

        suspectButton = view.findViewById(R.id.crime_suspect) as Button

        photoButton = view.findViewById(R.id.crime_camera) as ImageButton
        photoView = view.findViewById(R.id.crime_photo) as ImageView

        saveButton = view.findViewById(R.id.save_crime) as Button

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        crimeDetailViewModel.crimeLiveData.observe(
            viewLifecycleOwner,
            Observer { crime ->
                crime?.let {
                    this.crime = crime
                    photoFile = crimeDetailViewModel.getPhotoFile(crime.photoFileName)
                    photoUri = FileProvider.getUriForFile(
                        requireActivity(),
                        "com.ponykamni.criminalintent.fileprovider",
                        photoFile
                    )

                    updateUI()
                }
            })

        crimeDetailViewModel.crimeChanged.observe(
            viewLifecycleOwner,
            Observer { crimeChanged ->
                saveButton.visibility = if (crimeChanged) View.VISIBLE else View.GONE
            }
        )
    }

    override fun onStart() {
        super.onStart()
        val titleWatcher = object : TextWatcher {
            override fun beforeTextChanged(
                sequence: CharSequence?,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                sequence: CharSequence?,
                start: Int,
                before: Int,
                count: Int
            ) {
                crime.title = sequence.toString()
            }

            override fun afterTextChanged(sequence: Editable?) {
            }
        }
        titleField.addTextChangedListener(titleWatcher)

        solvedCheckBox.apply {
            setOnCheckedChangeListener { _, isChecked ->
                crime.isSolved = isChecked
            }
        }

        changeDate.setOnClickListener {
            DatePickerFragment.newInstance(crime.date).apply {
                setTargetFragment(this@CrimeFragment, REQUEST_DATE)
                show(this@CrimeFragment.parentFragmentManager, DIALOG_DATE)
            }
        }

        changeTime.setOnClickListener {
            TimePickerFragment.newInstance(crime.date).apply {
                setTargetFragment(this@CrimeFragment, REQUEST_TIME)
                show(this@CrimeFragment.parentFragmentManager, DIALOG_TIME)
            }
        }

        reportButton.setOnClickListener {
            Intent(Intent.ACTION_SEND).apply {
                type = "text/plain"
                putExtra(Intent.EXTRA_TEXT, getCrimeReport())
                putExtra(
                    Intent.EXTRA_SUBJECT,
                    getString(R.string.crime_report_subject)
                )
            }.also { intent ->
                val chooserIntent = Intent.createChooser(intent, getString(R.string.send_report))
                startActivity(chooserIntent)
            }
        }

        suspectButton.apply {
            val pickContactIntent =
                Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI)

            isEnabled = isThereAppToDoThis(pickContactIntent)

            setOnClickListener {
                startActivityForResult(pickContactIntent, REQUEST_CONTACT)
            }
        }

        photoButton.apply {
            val captureImage = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

            isEnabled = isThereAppToDoThis(captureImage)

            setOnClickListener {
                captureImage.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
                grantWriteUriPermissions(captureImage)
                startActivityForResult(captureImage, REQUEST_PHOTO)
            }
        }

        photoView.setOnClickListener {
            if (photoFile.exists()) {
                (requireActivity() as Callback).onPhotoPressed(photoFile.name)
            }
        }

        saveButton.setOnClickListener {
            crimeDetailViewModel.createOrUpdateCrime(this.crime)
        }
    }

    override fun onDateSelected(date: Date) {
        crime.date = questionableWayToMergeDateAndTimeFromTwoDifferentDateObjects(date, crime.date)
        updateUI()
    }

    override fun onTimeSelected(date: Date) {
        crime.date = questionableWayToMergeDateAndTimeFromTwoDifferentDateObjects(crime.date, date)
        updateUI()
    }

    private fun isThereAppToDoThis(intent: Intent): Boolean {
        val packageManager: PackageManager = requireActivity().packageManager
        val resolvedActivity: ResolveInfo? =
            packageManager.resolveActivity(
                intent,
                PackageManager.MATCH_DEFAULT_ONLY
            )

        return resolvedActivity != null
    }

    private fun grantWriteUriPermissions(intent: Intent) {
        val packageManager: PackageManager = requireActivity().packageManager
        val cameraActivities: List<ResolveInfo> =
            packageManager.queryIntentActivities(
                intent,
                PackageManager.MATCH_DEFAULT_ONLY
            )
        for (cameraActivity in cameraActivities) {
            requireActivity().grantUriPermission(
                cameraActivity.activityInfo.packageName,
                photoUri,
                Intent.FLAG_GRANT_WRITE_URI_PERMISSION
            )
        }
    }

    private fun questionableWayToMergeDateAndTimeFromTwoDifferentDateObjects(
        dateToRetrieveDateFrom: Date,
        dateToRetrieveTimeFrom: Date
    ): Date {
        val calendarWithDate = Calendar.getInstance().apply {
            time = dateToRetrieveDateFrom
        }
        val calendarWithTime = Calendar.getInstance().apply {
            time = dateToRetrieveTimeFrom
        }

        val year = calendarWithDate.get(Calendar.YEAR)
        val month = calendarWithDate.get(Calendar.MONTH)
        val day = calendarWithDate.get(Calendar.DAY_OF_MONTH)

        val hour = calendarWithTime.get(Calendar.HOUR_OF_DAY)
        val minute = calendarWithTime.get(Calendar.MINUTE)

        return GregorianCalendar(year, month, day, hour, minute).time
    }

    private fun updateUI() {
        titleField.setText(crime.title)
        dateField.text = getFormattedTimestamp(crime.date)
        solvedCheckBox.isChecked = crime.isSolved
        solvedCheckBox.apply {
            isChecked = crime.isSolved
            jumpDrawablesToCurrentState()
        }
        if (crime.suspect.isNotEmpty()) {
            suspectButton.text = crime.suspect
        }
        updatePhotoView()
    }

    private fun updatePhotoView() {
        if (photoFile.exists()) {
            val bitmap = BitmapScaler.getScaledBitmap(photoFile.path, requireActivity())
            photoView.setImageBitmap(bitmap)
            photoView.contentDescription = getString(R.string.crime_photo_image_description)
        } else {
            photoView.setImageDrawable(null)
            photoView.contentDescription = getString(R.string.crime_photo_no_image_description)
        }
    }

    private fun getCrimeReport(): String {
        val solvedString = if (crime.isSolved) {
            getString(R.string.crime_report_solved)
        } else {
            getString(R.string.crime_report_unsolved)
        }

        val dateString = DateFormat.getDateInstance(DateFormat.LONG).format(crime.date)

        val suspect = if (crime.suspect.isBlank()) {
            getString(R.string.crime_report_no_suspect)
        } else {
            getString(R.string.crime_report_suspect, crime.suspect)
        }

        return getString(
            R.string.crime_report,
            crime.title, dateString, solvedString, suspect
        )
    }

    private fun getFormattedTimestamp(date: Date): String {
        val dateString = DateFormat.getDateInstance(DateFormat.LONG).format(date)
        val timeString = DateFormat.getTimeInstance(DateFormat.SHORT).format(date)

        return "$dateString $timeString"
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when {
            resultCode != Activity.RESULT_OK -> return
            requestCode == REQUEST_CONTACT && data != null -> {
                val contactUri: Uri? = data.data
                val queryFields = arrayOf(ContactsContract.Contacts.DISPLAY_NAME)
                val cursor = contactUri?.let {
                    requireActivity().contentResolver
                        .query(contactUri, queryFields, null, null, null)
                }
                cursor?.use {
                    // Verify cursor contains at least one result
                    if (it.count == 0) {
                        return
                    }
                    it.moveToFirst()
                    val suspect = it.getString(0)
                    //TODO find a way to track changes
                    crime.suspect = suspect
                    suspectButton.text = suspect
                }
            }
            requestCode == REQUEST_PHOTO -> {
                requireActivity().revokeUriPermission(
                    photoUri,
                    Intent.FLAG_GRANT_WRITE_URI_PERMISSION
                )

                updatePhotoView()
            }
        }
    }

    companion object {
        private const val ARG_CRIME_ID = "crime_id"

        private const val DIALOG_DATE = "DialogDate"
        private const val DIALOG_TIME = "DialogTime"

        private const val REQUEST_DATE = 0
        private const val REQUEST_TIME = 1
        private const val REQUEST_CONTACT = 2
        private const val REQUEST_PHOTO = 3

        fun newInstance(crimeId: UUID?): CrimeFragment {
            val args = Bundle().apply {
                putSerializable(ARG_CRIME_ID, crimeId)
            }
            return CrimeFragment().apply {
                arguments = args
            }
        }
    }

    interface Callback {

        fun onPhotoPressed(fileName: String)
    }
}