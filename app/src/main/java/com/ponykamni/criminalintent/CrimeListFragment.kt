package com.ponykamni.criminalintent

import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.*
import com.ponykamni.criminalintent.list.SwipeToDeleteCallback
import java.text.DateFormat
import java.util.*

class CrimeListFragment : Fragment() {

    interface Callback {
        fun onCrimeSelected(crimeId: UUID)
        fun onCreateNewCrime()
    }

    private var callback: Callback? = null

    private val crimeListViewModel: CrimeListViewModel by lazy {
        ViewModelProvider(this).get(CrimeListViewModel::class.java)
    }

    private lateinit var crimeRecyclerView: RecyclerView
    private val adapter: CrimeAdapter = CrimeAdapter(DiffUtilItemCallback())

    private lateinit var listPlaceholder: LinearLayout
    private lateinit var addCrimePlaceholderButton: Button

    override fun onAttach(context: Context) {
        super.onAttach(context)
        callback = context as Callback?
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_crime_list, container, false)

        crimeRecyclerView = view.findViewById(R.id.crime_recycler_view) as RecyclerView
        crimeRecyclerView.layoutManager = LinearLayoutManager(context)
        crimeRecyclerView.adapter = adapter

        addFancyDeletionAnimationToRecyclerView(crimeRecyclerView)

        listPlaceholder = view.findViewById(R.id.list_placeholder)
        addCrimePlaceholderButton = view.findViewById(R.id.add_crime)

        addCrimePlaceholderButton.setOnClickListener {
            addNewCrime()
        }

        listPlaceholder.visibility = View.GONE

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        crimeListViewModel.crimeListLiveData.observe(
            viewLifecycleOwner,
            Observer { crimes ->
                crimes?.let {
                    adapter.submitList(crimes)
                    listPlaceholder.visibility = if (crimes.isEmpty()) View.VISIBLE else View.GONE
                }
            })
    }

    override fun onDetach() {
        super.onDetach()
        callback = null
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.fragment_crime_list, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.new_crime -> {
                addNewCrime()
                true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun addNewCrime() {
        callback?.onCreateNewCrime()
    }

    companion object {
        fun newInstance(): CrimeListFragment {
            return CrimeListFragment()
        }
    }

    private inner class CrimeHolder(view: View) : RecyclerView.ViewHolder(view),
        View.OnClickListener {

        private lateinit var crime: Crime

        private val titleTextView: TextView = itemView.findViewById(R.id.crime_title)
        private val dateTextView: TextView = itemView.findViewById(R.id.crime_date)
        private val crimeSolved: ImageView = itemView.findViewById(R.id.crime_solved)

        init {
            itemView.setOnClickListener(this)
        }

        fun bind(crime: Crime) {
            this.crime = crime
            titleTextView.text = crime.title
            dateTextView.text = getFormattedTimestamp(crime.date)
            crimeSolved.visibility = if (crime.isSolved) {
                View.VISIBLE
            } else {
                View.GONE
            }
        }

        private fun getFormattedTimestamp(date: Date): String {
            val dateString = DateFormat.getDateInstance(DateFormat.LONG).format(date)
            val timeString = DateFormat.getTimeInstance(DateFormat.SHORT).format(date)

            return "$dateString $timeString"
        }

        override fun onClick(v: View?) {
            callback?.onCrimeSelected(crime.id)
        }
    }

    private fun addFancyDeletionAnimationToRecyclerView(recyclerView: RecyclerView) {
        val swipeHandler = object : SwipeToDeleteCallback(this@CrimeListFragment.requireContext()) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val adapter = recyclerView.adapter as CrimeAdapter
                //todo find a better way
                crimeListViewModel.deleteCrime(adapter.currentList[viewHolder.adapterPosition])
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(recyclerView)
    }

    private inner class CrimeAdapter(callback: DiffUtilItemCallback) :
        ListAdapter<Crime, CrimeHolder>(callback) {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CrimeHolder {
            val view = layoutInflater.inflate(R.layout.list_item_crime, parent, false)
            return CrimeHolder(view)
        }

        override fun onBindViewHolder(holder: CrimeHolder, position: Int) {
            holder.bind(currentList[position])
        }
    }

    private inner class DiffUtilItemCallback : DiffUtil.ItemCallback<Crime>() {

        override fun areItemsTheSame(oldItem: Crime, newItem: Crime): Boolean =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: Crime, newItem: Crime): Boolean =
            oldItem == newItem

    }
}