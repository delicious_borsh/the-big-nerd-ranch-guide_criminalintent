package com.ponykamni.criminalintent

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import java.io.File

class PhotoDetailFragment : Fragment() {

    private lateinit var photoView: ImageView
    private lateinit var photoFile: File

    private val photoDetailViewModel by lazy {
        ViewModelProvider(this).get(PhotoDetailViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val photoFilename: String =
            arguments?.getSerializable(ARG_FILE_NAME) as String

        photoFile = photoDetailViewModel.getPhotoFile(photoFilename)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_photo_detail, container, false)

        photoView = view.findViewById(R.id.photo_detail) as ImageView

        if (photoFile.exists()) {
            val bitmap = BitmapScaler.getScaledBitmap(photoFile.path, requireActivity())
            photoView.setImageBitmap(bitmap)
        } else {
            photoView.setImageDrawable(null)
        }

        return view
    }

    companion object {
        private const val ARG_FILE_NAME = "crime_id"

        fun newInstance(photoFilename: String): PhotoDetailFragment {
            val args = Bundle().apply {
                putSerializable(ARG_FILE_NAME, photoFilename)
            }
            return PhotoDetailFragment().apply {
                arguments = args
            }
        }
    }
}