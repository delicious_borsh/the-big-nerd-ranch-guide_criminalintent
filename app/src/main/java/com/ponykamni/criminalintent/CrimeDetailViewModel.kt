package com.ponykamni.criminalintent

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import java.io.File
import java.util.*

class CrimeDetailViewModel : ViewModel() {

    private val crimeRepository = CrimeRepository.get()
    private val crimeIdLiveData = MutableLiveData<UUID>()

    val crimeChanged = MutableLiveData<Boolean>()

    var crimeLiveData: LiveData<Crime?> =
        Transformations.switchMap(crimeIdLiveData) { crimeId ->
            crimeRepository.getCrime(crimeId)
        }

    fun loadCrime(crimeId: UUID) {
        crimeIdLiveData.value = crimeId
    }

    fun createOrUpdateCrime(crime: Crime) {
        crimeRepository.createOrUpdateCrime(crime)
    }

    fun getPhotoFile(photoFileName: String): File {
        return crimeRepository.getPhotoFile(photoFileName)
    }

    fun onCrimeChanged() {
        crimeChanged.postValue(true)
    }

    fun onCrimeSaved() {
        crimeChanged.postValue(false)
    }
}