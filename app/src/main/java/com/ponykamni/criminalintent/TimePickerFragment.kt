package com.ponykamni.criminalintent

import android.app.Dialog
import android.app.TimePickerDialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import java.util.*

class TimePickerFragment : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val timeListener =
            TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->
                val resultDate: Date = GregorianCalendar(0, 0, 0, hourOfDay, minute).time

                targetFragment?.let { fragment ->
                    (fragment as Callback).onTimeSelected(resultDate)
                }
            }

        val date = arguments?.getSerializable(ARG_DATE) as? Date

        val calendar = Calendar.getInstance()

        if (date != null) {
            calendar.time = date
        }

        val initialHour = calendar.get(Calendar.HOUR_OF_DAY)
        val initialMinute = calendar.get(Calendar.MINUTE)

        return TimePickerDialog(
            requireContext(),
            timeListener,
            initialHour,
            initialMinute,
            false
        )
    }

    companion object {
        private const val ARG_DATE = "date"

        fun newInstance(date: Date): TimePickerFragment {
            val args = Bundle().apply {
                putSerializable(ARG_DATE, date)
            }
            return TimePickerFragment().apply {
                arguments = args
            }
        }
    }

    interface Callback {
        fun onTimeSelected(date: Date)
    }
}