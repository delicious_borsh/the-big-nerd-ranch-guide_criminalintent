package com.ponykamni.criminalintent

import androidx.lifecycle.ViewModel
import java.io.File

class PhotoDetailViewModel : ViewModel() {
    private val crimeRepository = CrimeRepository.get()

    fun getPhotoFile(photoFileName: String): File {
        return crimeRepository.getPhotoFile(photoFileName)
    }
}